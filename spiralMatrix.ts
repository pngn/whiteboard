// https://leetcode.com/problems/spiral-matrix/

import {
  assertEquals,
  assertThrows,
} from "https://deno.land/std@0.123.0/testing/asserts.ts";

/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
function spiralOrder(matrix: number[][]): number[] {
  // the matrix has m rows and n columns
  const m = matrix.length;
  if (m === 0) {
    throw new Error("Invalid input matrix");
  }
  const n = matrix[0].length;

  const output = [];

  // for each level of depth
  let d = 0;

  while (true) {
    // calculate the length of the rows and columns that we want to process
    const rows = m - d * 2;
    const columns = n - d * 2;

    // if there are no rows left then we are done
    if (rows <= 0 || columns <= 0) {
      break;
    }

    // add the north row which runs from (d, d) to (d, n - d)
    for (let i = d; i < n - d; i++) {
      output.push(matrix[d][i]);
    }

    // add the east column which runs from (d + 1, n - d) to (m - d - 1, n - d)
    for (let i = d + 1; i < m - d - 1; i++) {
      output.push(matrix[i][n - d - 1]);
    }

    // if there is a top and bottom row
    if (rows > 1) {
      // add the south row which runs from  (m - d, n - d) to (m - d, d)
      for (let i = n - d - 1; i >= d; i--) {
        output.push(matrix[m - d - 1][i]);
      }
    }

    // if there is a top and bottom column
    if (columns > 1) {
      // add the west column which runs from  (m - d - 1, d) to (d + 1, d)
      for (let i = m - d - 2; i >= d + 1; i--) {
        output.push(matrix[i][d]);
      }
    }

    // let's go deeper
    d++;
  }

  return output;
}

Deno.test("it handles bad matrices", () => {
  assertThrows(() => spiralOrder([]));
  assertEquals(spiralOrder([[]]), []);
});

Deno.test("it handles the base case", () => {
  assertEquals(spiralOrder([[6]]), [6]);
});

Deno.test("it handles a single row or column", () => {
  assertEquals(spiralOrder([[6, 5]]), [6, 5]);
  assertEquals(spiralOrder([[4], [3]]), [4, 3]);
});

Deno.test("it handles a non-trivial matrices", () => {
  assertEquals(spiralOrder([[6, 5], [3, 4]]), [6, 5, 4, 3]);
  assertEquals(spiralOrder([[6, 5, 4], [1, 2, 3]]), [6, 5, 4, 3, 2, 1]);
});

Deno.test("it handles matrices with multiple levels of depth", () => {
  assertEquals(
    spiralOrder(
      [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]],
    ),
    [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7],
  );

  assertEquals(
    spiralOrder(
      [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]],
    ),
    [1, 2, 3, 6, 9, 12, 11, 10, 7, 4, 5, 8],
  );
});
