// https://leetcode.com/problems/reorder-list/
import { assertEquals } from "https://deno.land/std@0.123.0/testing/asserts.ts";

function reorderList(head: LinkedListNode<number> | null) {
  // add a reverse link to each element in the list

  // starting at the head, the previous element is null
  let current = head;
  let previous = null;

  // whilst the current node is not null
  while (current !== null) {
    // update the previous for the current node
    current.previous = previous;

    // update the previous and current pointers
    previous = current;
    current = current.next;
  }

  // reset our pointers
  // the tail is now where we were one step before!
  let left = head;
  let right = previous;

  const newHead = buildNode(0);
  current = newHead;

  // when the pointers touch we are done
  while (left !== right) {
    current = buildNode(left!.value);
    current.next = buildNode(right!.value);
  }

  return newHead.next;
}

Deno.test("linked list translations work", () => {
  assertEquals(fromLinkedList(toLinkedList([])), []);

  const linkedList = {
    value: 1,
    next: {
      value: 2,
      next: { value: 3, next: null, previous: null },
      previous: null,
    },
    previous: null,
  };
  const recreated = toLinkedList(fromLinkedList(linkedList));
  assertEquals(recreated!.value, 1);
  assertEquals(recreated!.next!.value, 2);
  assertEquals(recreated!.next!.next!.value, 3);
});

Deno.test("simple lists work", () => {
  assertEquals(fromLinkedList(reorderList(toLinkedList([]))), []);
  assertEquals(fromLinkedList(reorderList(toLinkedList([1]))), [1]);
  assertEquals(fromLinkedList(reorderList(toLinkedList([1]))), [1]);
});

Deno.test("the base case works", () => {
  assertEquals(
    fromLinkedList(reorderList(toLinkedList([1, 2, 3, 4]))),
    [1, 3, 2, 4],
  );
});

// Deno.test("the more advanced case works", () => {
//   assertEquals(
//     fromLinkedList(reorderList(toLinkedList([1, 2, 3, 4, 5]))),
//     [1, 5, 2, 4, 3],
//   );
// });

interface LinkedListNode<T> {
  value: T;
  next: LinkedListNode<T> | null;
  previous: LinkedListNode<T> | null;
}

function buildNode<T>(value: T): LinkedListNode<T> {
  return { value, next: null, previous: null };
}

function toLinkedList<T>(list: T[]): LinkedListNode<T> | null {
  let head: LinkedListNode<T> | null = null;
  let current: LinkedListNode<T> | null = null;

  for (const value of list) {
    const node = buildNode(value);
    if (head === null) {
      head = node;
      current = node;
    } else {
      current!.next = node;
      current = node;
    }
  }
  return head;
}

function fromLinkedList<T>(head: LinkedListNode<T> | null): T[] {
  const list = [];

  while (head) {
    list.push(head.value);
    head = head.next;
  }

  return list;
}
