// https://leetcode.com/problems/word-search/description/

import { assertEquals } from "https://deno.land/std@0.123.0/testing/asserts.ts";

type Node = { letter: string; neighbours: Map<string, Node[]> };

function wordExists(grid: string[][], word: string): boolean {
  const m = grid.length;
  if (m === 0) {
    return false;
  }
  const n = grid[0].length;
  if (n === 0) {
    return false;
  }
  if (word.length === 0) {
    return false;
  }

  // build a graph that represents the grid
  const positions = new Map<string, Node>();
  // nasty hack to ensure that we have an equality method on grid coordinates
  const tokenize = ([i, j]: [number, number]): string => `${i}-${j}`;

  // create a fake grid position for the first letter
  const origin: Node = { letter: "", neighbours: new Map<string, Node[]>() };

  // create a Node object for each letter in the grid
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      const letter = grid[j][i];

      // create a node for this position in the grid
      const node = {
        letter,
        neighbours: new Map<string, Node[]>(), // absolutely egregious optimisation
      };

      // store a reference to this node by its position
      positions.set(tokenize([i, j]), node);

      // add the node as a neighbour to the origin node
      origin.neighbours.set(letter, [
        ...(origin.neighbours.get(letter) ?? []),
        node,
      ]);
    }
  }

  // join our Node objects up to their neighbours
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      const node: Node = positions.get(tokenize([i, j])) as Node;
      const found: Node[] = [];

      // add N = (i, j - 1)
      if (j - 1 >= 0) {
        found.push(positions.get(tokenize([i, j - 1])) as Node);
      }

      // add E = (i + 1, j)
      if (i + 1 < n) {
        found.push(positions.get(tokenize([i + 1, j])) as Node);
      }

      // add S = (i, j + 1)
      if (j + 1 < m) {
        found.push(positions.get(tokenize([i, j + 1])) as Node);
      }

      // add W = (i - 1, j)
      if (i - 1 >= 0) {
        found.push(positions.get(tokenize([i - 1, j])) as Node);
      }

      found.forEach((neighbour) => {
        const letter = neighbour.letter; // the letter that we found in the neighbours.
        node.neighbours.set(letter, [
          ...(node.neighbours.get(letter) ?? []),
          neighbour,
        ]);
      });
    }
  }

  // perform BFS on our search problem
  let candidates: Node[] = [origin];
  const seen: Set<Node> = new Set();
  for (let i = 0; i < word.length; i++) {
    const letter = word[i];

    // get the next set of candidates for the current level
    candidates.forEach((candidate) => seen.add(candidate));
    candidates = candidates
      .flatMap((node) => node.neighbours.get(letter) ?? [])
      .filter((candidate) => !seen.has(candidate));

    if (candidates.length === 0) {
      // game over man, game over
      return false;
    }
  }

  return true;
}

Deno.test("it handles bad grids", () => {
  assertEquals(wordExists([], "hello"), false);
  assertEquals(wordExists([[]], "hello"), false);
  assertEquals(wordExists([["h"]], ""), false);
});

Deno.test("it handles the base case", () => {
  assertEquals(wordExists([["h"]], "h"), true);
});

Deno.test("it returns true if it can find the word", () => {
  const grid = [
    ["h", "e"],
    ["t", "a"],
  ];
  assertEquals(wordExists(grid, "h"), true);
  assertEquals(wordExists(grid, "he"), true);
  assertEquals(wordExists(grid, "hea"), true);
  assertEquals(wordExists(grid, "heat"), true);
  assertEquals(wordExists(grid, "thea"), true);
});

Deno.test("it returns false if it can't find the word", () => {
  const grid = [
    ["h", "e"],
    ["t", "a"],
  ];

  assertEquals(wordExists(grid, "n"), false, "found n");
  assertEquals(wordExists(grid, "neat"), false, "found neat");
  assertEquals(wordExists(grid, "het"), false, "found het");
  assertEquals(wordExists(grid, "heata"), false, "found heata");
});

Deno.test("it handles the example test case", () => {
  const grid = [
    ["A", "B", "C", "E"],
    ["S", "F", "C", "S"],
    ["A", "D", "E", "E"],
  ];
  assertEquals(wordExists(grid, "ABCCED"), true);
  assertEquals(wordExists(grid, "SEE"), true);
  assertEquals(wordExists(grid, "ABCB"), false, "found ABCB");
});
