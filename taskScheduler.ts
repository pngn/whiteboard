// https://leetcode.com/problems/task-scheduler/

// returns the minimum units of time required to execute all of the specified tasks
export function leastTime(tasks: string, cooldown: number): number {
  const counts: { [task: string]: number } = {};
  console.log("\nStarting", tasks, cooldown);

  for (let i = 0; i < tasks.length; i++) {
    const task = tasks[i];
    counts[task] = (counts[task] ?? 0) + 1;
  }

  // an array of [task, count] sorted by count
  const sortedTasks = Object.entries(counts).sort((a, b) => b[1] - a[1]);

  let remaining = tasks.length;
  const schedule = [];
  const lastSeen: { [task: string]: number } = {};

  while (remaining > 0) {
    let added = false;

    // iterate through the tasks in priority order
    for (let i = 0; i < sortedTasks.length; i++) {
      const [task, count] = sortedTasks[i];

      // don't execute the task if it is still on cooldown
      if (
        count > 0 &&
        (lastSeen[task] === undefined ||
          schedule.length - lastSeen[task] >= cooldown)
      ) {
        // execute the task and update counts and cooldowns
        schedule.push(task);
        sortedTasks[i] = [task, count - 1];
        lastSeen[task] = schedule.length;
        remaining -= 1;
        added = true;

        // go back to trying to schedule the most common task
        // TODO handle being dominated by small tasks
        break;
      }
    }

    // idle the machine because there's nothing to do
    if (!added) {
      schedule.push("I");
    }
  }

  console.log(schedule.join(""));

  return schedule.length;
}
