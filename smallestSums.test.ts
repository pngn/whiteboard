import { smallestSumsGraphSearch } from "./smallestSums.ts";

import { assertEquals } from "https://deno.land/std@0.123.0/testing/asserts.ts";

Deno.test("smallestSumsGraphSearch returns the trivial answer", async () => {
  assertEquals(smallestSumsGraphSearch([1], [1], 3), [2]);
  assertEquals(smallestSumsGraphSearch([1, 1, 2], [1, 2, 3], 3), [2, 2, 3]);
  assertEquals(smallestSumsGraphSearch([1, 1, 2], [1, 2, 3], 9), [
    2,
    2,
    3,
    3,
    3,
    4,
    4,
    4,
    5,
  ]);
});
