# whiteboard

Some attempts at whiteboard questions for interview preparation.

## Installation

Install a copy of deno (tested on v1.18.1).

## Development

Run the unit tests

```
deno test
```
