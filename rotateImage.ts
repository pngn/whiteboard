// https://leetcode.com/problems/rotate-image/

import {
  assertEquals,
  assertThrows,
} from "https://deno.land/std@0.123.0/testing/asserts.ts";

/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
function rotateImage(m: number[][]): number[][] {
  // the matrix has m rows and n columns
  const n = m.length;
  if (n === 0) {
    throw new Error("Invalid input matrix");
  }

  // for each level of depth
  let d = 0;

  // whilst there are still rows left to process
  while (n - d * 2 > 0) {
    // the outside edge runs from (d, d) to (d, n - d - 1)
    for (let i = d; i < n - d - 1; i++) {
      // the coordinates of each element is
      // N (d, d + i)
      // E (d + i, n - d)
      // S (n - d, n - d - i)
      // W (n - d - i, d)

      // set temp to be the value of W
      const temp = m[n - 1 - d - i][d];

      // set W to the value of S
      m[n - 1 - d - i][d] = m[n - 1 - d][n - 1 - d - i];

      // set S to the value of E
      m[n - 1 - d][n - 1 - d - i] = m[d + i][n - 1 - d];

      // set E to the value of N
      m[d + i][n - 1 - d] = m[d][d + i];

      // set N to the value of temp
      m[d][d + i] = temp;
    }

    // let's go deeper
    d++;
  }

  return m;
}

Deno.test("it handles bad matrices", () => {
  assertThrows(() => rotateImage([]));
  assertEquals(rotateImage([[]]), [[]]);
});

Deno.test("it handles the base case", () => {
  assertEquals(rotateImage([[6]]), [[6]]);
});

Deno.test("it handles 2x2 matrices", () => {
  assertEquals(
    rotateImage([
      [1, 2],
      [4, 3],
    ]),
    [
      [4, 1],
      [3, 2],
    ],
  );
});

Deno.test("it handles 3x3 matrices", () => {
  assertEquals(
    rotateImage([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ]),
    [
      [7, 4, 1],
      [8, 5, 2],
      [9, 6, 3],
    ],
  );
});

Deno.test("it handles 4x4 matrices", () => {
  assertEquals(
    rotateImage([
      [5, 1, 9, 11],
      [2, 4, 8, 10],
      [13, 3, 6, 7],
      [15, 14, 12, 16],
    ]),
    [
      [15, 13, 2, 5],
      [14, 3, 4, 1],
      [12, 6, 8, 9],
      [16, 7, 10, 11],
    ],
  );
});
