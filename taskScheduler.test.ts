import { assertEquals } from "https://deno.land/std@0.123.0/testing/asserts.ts";

import { leastTime } from "./taskScheduler.ts";

Deno.test("leastTime is zero for no tasks", () => {
  assertEquals(leastTime("", 0), 0);
});

Deno.test("leastTime is always 1 for one task", () => {
  assertEquals(leastTime("A", 0), 1);
  assertEquals(leastTime("A", 1), 1);
  assertEquals(leastTime("A", 2), 1);
});

Deno.test("leastTime depends on the cooldown", () => {
  assertEquals(leastTime("AA", 0), 2);
  assertEquals(leastTime("AA", 1), 3);
  assertEquals(leastTime("AA", 2), 4);
});

Deno.test("leastTime intersperses tasks", () => {
  assertEquals(leastTime("AAABBB", 2), 8);
});

Deno.test("can be dominated by one task", () => {
  assertEquals(leastTime("AAAAAABCDEFG", 2), 16);
});

Deno.test("can be dominated by small tasks", () => {
  // ABCDABCDA is valid
  assertEquals(leastTime("AAABBCCDD", 2), 9);
});
