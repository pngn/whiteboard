// https://leetcode.com/problems/find-median-from-data-stream/
import { assertEquals } from "https://deno.land/std@0.123.0/testing/asserts.ts";

class MedianFinder {
  private left: number[] = [];
  private right: number[] = [];

  addNum(num: number): void {
    if (this.left.length > this.right.length) {
      this.right.push(num);
    } else {
      this.left.push(num);
    }
  }

  findMedian(): number | null {
    // return null if no numbers have been added yet
    if (this.left.length === 0) {
      return null;
    }

    // find the left and right values of the middle position
    // this is in case the length is odd
    // if the length is even then both will be the same
    const left = Math.floor(middle);
    const right = Math.ceil(middle);

    // get the numbers in position left and position right and average them
    // this is the median
    const a = this.nums[left];
    const b = this.nums[right];
    const average = (a + b) / 2;

    return average;
  }
}

Deno.test("median is null for no data", () => {
  assertEquals(new MedianFinder().findMedian(), null);
});

Deno.test("median is the value for a single value", () => {
  const medianFinder = new MedianFinder();
  medianFinder.addNum(44);
  assertEquals(medianFinder.findMedian(), 44);
});

Deno.test("handles example test", () => {
  const medianFinder = new MedianFinder();
  medianFinder.addNum(1);
  medianFinder.addNum(2);
  assertEquals(medianFinder.findMedian(), 1.5);

  medianFinder.addNum(3);
  assertEquals(medianFinder.findMedian(), 2.0);
});

Deno.test("handles input that isn't sorted", () => {
  const medianFinder = new MedianFinder();
  medianFinder.addNum(3);
  medianFinder.addNum(1);
  assertEquals(medianFinder.findMedian(), 2.0);

  medianFinder.addNum(2);
  assertEquals(medianFinder.findMedian(), 2.0);
});

Deno.test("handles longer input that isn't sorted", () => {
  const medianFinder = new MedianFinder();
  medianFinder.addNum(3);
  medianFinder.addNum(1);
  medianFinder.addNum(2);
  medianFinder.addNum(7);
  medianFinder.addNum(8);
  medianFinder.addNum(9);
  medianFinder.addNum(2);
  medianFinder.addNum(4);
  medianFinder.addNum(5);
  assertEquals(medianFinder.findMedian(), 5.0);
});
