import { assertEquals } from "https://deno.land/std@0.123.0/testing/asserts.ts";

import { maxVolume } from "./container.ts";

Deno.test("maxVolume is zero for no data", () => {
  assertEquals(maxVolume([]), 0);
  assertEquals(maxVolume([10]), 0);
});

Deno.test("maxVolume handles the base case", () => {
  assertEquals(maxVolume([10, 10]), 10);
  assertEquals(maxVolume([20, 10]), 10);
});

Deno.test("maxVolume finds large containers in the middle", () => {
  assertEquals(maxVolume([5, 20, 20, 5]), 20);
  assertEquals(maxVolume([1, 8, 6, 2, 5, 4, 8, 3, 7]), 49);
});
