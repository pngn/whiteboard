// https://leetcode.com/problems/container-with-most-water/

export function maxVolume(data: number[]): number {
  // if the data is not large enough to hold water then we are done
  if (data.length < 2) {
    return 0;
  }

  // start with two pointers on either side of the dataset
  let l = 0;
  let r = data.length - 1;

  // keep track of the best volume we've achieved so far
  let maxVolume = -Infinity;

  // until the two ends have met
  while (r > l) {
    // calculate the volume of a given moment
    const width = r - l;
    const height = Math.min(data[l], data[r]);
    const volume = width * height;

    // update the max volume if applicable
    if (volume > maxVolume) {
      maxVolume = volume;
    }

    // move the smaller of the two pointers
    if (data[l] > data[r]) {
      // move the right pointer in
      r -= 1;
    } else {
      // move the left pointer up
      l += 1;
    }
  }

  return maxVolume;
}
