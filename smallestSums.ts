// https://leetcode.com/problems/find-k-pairs-with-smallest-sums/

type Coordinate = [number, number];

export function smallestSumsGraphSearch(
  a: number[],
  b: number[],
  k: number,
): number[] {
  // a list of coordinates that we want to explore
  // start at the top-left corner
  let candidates: Coordinate[] = [[0, 0]];

  // the solutions we've found so far
  const solutions: number[] = [];

  // avoid recording coordinates twice
  let seen: Set<string> = new Set();

  // whilst we still have more candidates to explore
  while (solutions.length < k) {
    // if there are no candidates then we have exhausted the graph
    // this suggests that k was impossibly high
    if (candidates.length === 0) {
      break;
    }

    // find the entries at the current level
    // TODO use a min heap here ;)
    let current: Coordinate[] = [];
    let lowestSum = Infinity;
    for (let i = 0; i < candidates.length; i++) {
      const candidate = candidates[i];
      const x = candidate[0];
      const y = candidate[1];
      const sum = a[x] + b[y];
      if (sum < lowestSum) {
        lowestSum = sum;
      }
    }

    // build the next set of candidates
    const next: Coordinate[] = [];

    for (let i = 0; i < candidates.length; i++) {
      const candidate = candidates[i];
      const x = candidate[0];
      const y = candidate[1];
      const sum = a[x] + b[y];

      if (sum === lowestSum) {
        // we have a solution
        solutions.push(sum);

        if (solutions.length === k) {
          return solutions;
        }

        // add the neighbours of the solution to the candidate pool
        if (x + 1 < a.length && !seen.has(`${x + 1}-${y}`)) {
          next.push([x + 1, y]);
          seen.add(`${x + 1}-${y}`);
        }
        if (y + 1 < b.length && !seen.has(`${x}-${y + 1}`)) {
          next.push([x, y + 1]);
          seen.add(`${x}-${y + 1}`);
        }
      } else {
        // we don't have a solution, it continues to be a candidate
        next.push(candidate);
      }
    }

    // use the next level of candidates
    candidates = next;
  }

  // the graph search was not able to find k pairs so return the solutions that we have
  return solutions;
}
